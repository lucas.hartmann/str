#include <iostream>
#include <pthread.h>
#include <unistd.h>

typedef void *(*thread_fcn_t)(void*);

using namespace std;

pthread_spinlock_t sl;
pthread_mutex_t mtx;

void *thread(void *) {
    usleep(100);
    cout << "[THREAD] Waiting for the spinlock..." << endl;
    pthread_spin_lock(&sl);
    cout << "[THREAD] Acquired spinlock." << endl;

    cout << "[THREAD] Waiting for the mutex..." << endl;
    pthread_mutex_lock(&mtx);
    cout << "[THREAD] Acquired mutex." << endl;

    return 0;
}

int main() {
    cout << "Initialize spinlock..." << endl;
    if (pthread_spin_init(&sl, PTHREAD_PROCESS_PRIVATE)) {
        cout << "Failed." << endl;
        return 1;
    }
    pthread_spin_lock(&sl);

    cout << "Initialize mutex..." << endl;
    if (pthread_mutex_init(&mtx, 0)) {
        cout << "Failed." << endl;
        return 1;
    }
    pthread_mutex_lock(&mtx);

    cout << "Start parallel thread..." << endl;
    pthread_t th;
    if (pthread_create(&th, 0, thread, 0)) {

    }

    sleep(15);
    cout << "Releasing spinlock..." << endl;
    pthread_spin_unlock(&sl);

    sleep(15);
    cout << "Releasing spinlock..." << endl;
    pthread_mutex_unlock(&mtx);

    cout << "Joining thread..." << endl;
    pthread_join(th, 0);
    cout << "Done." << endl;

    return 0;
}
