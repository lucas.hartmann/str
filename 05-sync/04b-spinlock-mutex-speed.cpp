#include <iostream>
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h>

typedef void *(*thread_fcn_t)(void*);

using namespace std;

uint64_t nanos() {
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec*1000000000ULL + ts.tv_nsec;
}

pthread_spinlock_t sl;
pthread_mutex_t mtx;

int main() {
    uint64_t N, then, now;

    cout << "Timing spinlock... " << flush;
    if (pthread_spin_init(&sl, PTHREAD_PROCESS_PRIVATE)) {
        cout << "Failed." << endl;
        return 1;
    }
    N = 100000000;
    then = nanos();
    while (N--) {
        pthread_spin_lock(&sl);
        pthread_spin_unlock(&sl);
    }
    now = nanos();
    cout << (now-then)/1000000. << "ms / 100M locks." << endl;

    cout << "Timing mutex... " << flush;
    N = 100000000;
    if (pthread_mutex_init(&mtx, 0)) {
        cout << "Failed." << endl;
        return 1;
    }
    while (N--) {
        pthread_mutex_lock(&mtx);
        pthread_mutex_unlock(&mtx);
    }
    now = nanos();
    cout << (now-then)/1000000. << "ms / 100M locks." << endl;

    return 0;
}
