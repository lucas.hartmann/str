SUBDIRS += 00-args
SUBDIRS += 01-memory-layout
SUBDIRS += 02-fork
SUBDIRS += 03-ipc
SUBDIRS += 04-threads
SUBDIRS += 05-sync
SUBDIRS += 06-sched
SUBDIRS += 07-mem
SUBDIRS += 07-rt

.PHONY: subdirs $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

clean:
	for i in  $(SUBDIRS); do \
		make -C $$i clean; \
	done \
