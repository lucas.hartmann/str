/* DEE / CEAR / UFPB
 * Realtime Systems: Shared memory example, part 1: Memory mapping files.
 *
 * (c) 2022 Lucas V. Hartmann <gitlab.com/lucas.hartmann>
 */
#include <iostream>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

using namespace std;

// Assertions are checks on a condition.
// If true then assume success, no action required.
// If false then assume error, print message, and abort program.
void assert(bool should_be_true, const char *msg) {
    if (should_be_true)
        return;

    cerr << msg << endl;
    exit(1);
}

int main() {
    int err;

    // Opens a file for reading and writing.
    // Create it if necessary, with rw-r--r-- permissions.
    int fd = open("test_file.dat", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    assert(fd != -1, "open(...) failed.");

    // Resize to 4096 bytes.
    err = ftruncate(fd, 4096);
    assert(err != -1, "ftruncate(...) failed.");

    // Map the file contents into memory.
    // Note that mmap returns a void*, i.e. a pointer with "unknown" data type.
    void *vp = mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    assert(vp != 0, "mmap(...) failed.");
    cout << "File contents mapped to RAM at 0x" << vp << endl;

    // Change pointer data type to "bytes."
    uint8_t *u8p = (uint8_t *)vp;

    cout << "Open a second terminal window and monitor the contents of test_file.dat. Try:" << endl;
    cout << "while sleep 0.1; do clear; hexdump -C test_file.dat; done" << endl;
    cout << endl;

    // main loop
    while (true) {
        int i;
        cout << "Enter 0-1023 to increment the byte at that offset: ";
        cin >> i;

        // Stop looping on invalid entries;
        if (!cin || i<0 || i>=4096) break;

        u8p[i]++;
    };

    return 0;
}
