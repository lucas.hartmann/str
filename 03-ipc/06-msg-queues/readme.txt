To run this example:

1 - Run make to compile everything.
2 - Start worker in a terminal.
3 - Start reader in another terminal.
4 - Start writer in yet another terminal.
5 - Type messages in writer, and observe the output from reader.

Worker creates and initializes a couple of message queues:
- *_lc: Contains input messages (loweracase text).
- *_uc: Contains output messages (uppercase text).

Writer does:
- Reads a message.
- Send message.
- Repeat.

Worker does:
- Wait for input message.
- Works on the message (converts to uppercase).
- Sends output message (uppercase).
- Repeat.

Reader does:
- Wait for message;
- Print the message.
- Repeat.

The effects are basically the same as with shared memory, except
that message queues already include the notification logic.
